#!/usr/bin/env
import requests
import datetime

ASTRONOMYAPI_ID="fc72679b-71a9-439b-bed3-f221a891de50"
ASTRONOMYAPI_SECRET="cd5cdd6cc0e6d65110124d71c9d9b53ab174ea99e39134900fb348c5b1af1e06a5c9ff27d7c7e7f0cfcb86db4952c7dff0c4359db62c903b36b7d06f183318540e493a80305c108fb9d48a702ef8b9061eb1fac965b60e397ca2295868c4ac46874381fd4f1774fb2a135c68ee7a5168"

def get_observer_location():
        """Returns long and lat for the location of this machine

        Returns:
        str: lat
        str:long"""
        response = requests.get('http://ip-api.com/json/165.225.220.250?fields=lat,lon')
        events = response.json()
        latitude = events['lat']
        longitude = events['lon']
        print(latitude, longitude)
        return latitude, longitude

def get_sun_position(latitude, longitude):
    """Returns the current position of the sun in the sky at the specified location

    Parameters:
    latitude (str)
    longitude (str)

    Returns:
    float: azimuth
    float: altitude
    """
    get_observer_location()
    payload = 
    request = requests.get('https://astronomyapi.com', auth=(ASTRONOMYAPI_ID, ASTRONOMYAPI_SECRET))

    return 0, 0

def print_position(azimuth, altitude):
    """Prints the position of the sun in the sky usign the supplied coordinates

    Parameters:
    azimuth (float)
    altitude (float)"""

    print("The Sun is currently at:")

if __name__ == "__main__":
    latitude, longitude = get_observer_location()
    azimuth, altitude = get_sun_position()
    print_position(azimuth, altitude)
